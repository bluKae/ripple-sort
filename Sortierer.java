/* Sortierer.java version 1.0.0
 * Copyright (C) 2022  bluKae
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class Sortierer {
    private int[] feld;
  
    public Sortierer(int[] feld) {
        this.feld = feld;
    }
  
  
    public void rippleSort() {
        for (int i = 0; i < feld.length - 1; i++) {
            for (int j = i; j < feld.length; j++) {
                if (feld[j] < feld[i]) {
                    tauschen(i, j);
                }  
            }
        }
    }
  
    public void tauschen(int a, int b) {
        int tmp = feld[a];
        feld[a] = feld[b];
        feld[b] = tmp;
    }
  
    public String toString() {
        return Arrays.toString(feld);
    }         
  
    
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
  
        int[] feld = new int[10];
        String input;
  
        do {
            for (int i = 0; i < feld.length; i++) {
                feld[i] = random.nextInt(100);  
            }
            
            Sortierer sortierer = new Sortierer(feld);
            
            System.out.println("Unsortiert: " + sortierer.toString());
            sortierer.rippleSort();
            System.out.println("Sortiert: " + sortierer.toString());
            
            input = scanner.nextLine();
        } while (!input.equals("fertig"));                 
    }
}
